<?php

namespace designerei\ContaoHyperlinkExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ContaoHyperlinkExtendedListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        if ('ce_hyperlink' === $template->getName()) {

            // check button styles
            if ($template->buttonStyle === 'custom') {
                $template->buttonStyle = '';

            } else {
                $template->buttonStyle = 'btn-' . $template->buttonStyle;
            }

            // add full width to button class
            if ($template->fullwidth && $template->buttonClass) {
                $template->buttonClass .= ' w-full';
            } elseif ($template->fullwidth) {
                $template->buttonClass .= 'w-full';
            }
        }
    }
}
