<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'useButton';

$GLOBALS['TL_DCA']['tl_content']['subpalettes']['useButton'] =
  'buttonStyle, buttonClass, fullwidth'
;

$GLOBALS['TL_DCA']['tl_content']['fields']['useButton'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('submitOnChange'=>true),
    'sql' => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['buttonStyle'] = [
    'inputType' => 'select',
    'options' => array('default', 'accent', 'danger', 'custom'),
    'default' => 'default',
    'reference' => &$GLOBALS['TL_LANG']['tl_content']['buttonStyle'],
    'eval' => array('tl_class'=>'w50'),
    'sql' => "varchar(32) NOT NULL default"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['buttonClass'] = [
    'inputType' => 'text',
    'eval' => array('tl_class'=>'w50'),
    'sql'  => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['fullwidth'] = [
    'inputType' => 'checkbox',
    'eval' => array('tl_class'=>'w50'),
    'sql' => "char(1) NOT NULL default ''"
];


PaletteManipulator::create()
    ->removeField('useImage', 'imglink_legend')
    ->addLegend('button_legend', 'link_legend', PaletteManipulator::POSITION_AFTER)
    ->addField('useButton', 'button_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('hyperlink', 'tl_content')
;
